export interface UserBody {
  email: string;
  userId: string;
  name: string;
  image?: string;
  repos?: [];
}

export type GetUserProps = {
  userId: string;
};

export type UpdateUserProps = {
  apiKey: string;
  userId: string;
};

export type UseUserApiResponse = {
  updateUser: (updateUserProps: UpdateUserProps) => Promise<void>;
  getUser: (getUserProps: GetUserProps) => Promise<UserBody>;
};

////

export type ReviewBodyProps = {
  url: string;
};

export type ReviewResponseProps = {
  data: string;
};

export type FixBugBodyProps = {
  stacktrace: string;
  code: string;
};

export type FixBugResponseProps = {
  data: string;
};

export type DemoApiResponse = {
    reviewCode: (reviewBodyProps: ReviewBodyProps) => Promise<ReviewResponseProps>;
    fixBug: (fixBugBodyProps: FixBugBodyProps) => Promise<FixBugResponseProps>;
}
////
