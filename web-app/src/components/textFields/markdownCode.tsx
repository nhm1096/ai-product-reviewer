import React, { useEffect, useRef, useState } from "react";
import ReactMarkdown from "react-markdown";
import { useSpring, animated } from "react-spring";
import { Prism as SyntaxHighlighter } from "react-syntax-highlighter";
import { oneDark } from "react-syntax-highlighter/dist/esm/styles/prism";
type MarkdownCodeProps = {
  message: string;
  isHidden: boolean;
};

const MarkdownCode = ({
  message,
  isHidden,
}: MarkdownCodeProps): JSX.Element => {
  const disableAutoScroll = useRef<boolean>(true);
  const scrollYCache = useRef<number>(0);
  const [revealedMessage, setRevealedMessage] = useState("");
  const [showMessage, setShowMessage] = useState(false);
  const [triggerScroll, setTriggerScroll] = useState(true);

  useEffect(() => {
    const handleScroll = (ev: Event) => {
      const scrollY = window.scrollY;

      // FIXME: hack
      setTriggerScroll(scrollY >= scrollYCache.current);

      scrollYCache.current = scrollY;
    };

    window.addEventListener("scroll", handleScroll);
    return () => {
      window.removeEventListener("scroll", handleScroll);
    };
  }, []);

  useEffect(() => {
    if (triggerScroll && !disableAutoScroll.current) {
      window.scrollTo(0, document.body.scrollHeight);
    }
  }, [revealedMessage, triggerScroll]);

  useEffect(() => {
    disableAutoScroll.current = false;
    let timer: NodeJS.Timer;

    setShowMessage(true);

    let i = 0;
    let tmp = '';
    setRevealedMessage(tmp);

    timer = setInterval(() => {
      if (i < message.length) {
        tmp += message.charAt(i);
        setRevealedMessage(tmp);
        ++i;
      } else {
        disableAutoScroll.current = true;
        clearInterval(timer);
      }
    });

    return () => {
      clearInterval(timer);
    };
  }, [message]);

  if (isHidden || message.length === 0) {
    return <></>;
  }

  const springProps = useSpring({
    opacity: 1,
    transform: "translateY(0px)",
    from: {
      opacity: 0,
      transform: "translateY(50px)",
    },
  });

  return (
    <animated.div
      style={springProps}
      className="flex flex-col m-10 p-5 bg-white font-mono p-3 w-[85%] text-sm text-gray-600 rounded-md border-2 border-black"
    >
      {showMessage && (
        <ReactMarkdown
          className="markdown"
          components={{
            code({ node, inline, className, children, ...props }) {
              const match = /language-(\w+)/.exec(className || "");
              return !inline && match ? (
                <SyntaxHighlighter
                  {...props}
                  children={String(children).replace(/\n$/, "")}
                  style={oneDark}
                  language={match[1]}
                  PreTag="div"
                />
              ) : (
                <code {...props} className={className}>
                  {children}
                </code>
              );
            },
          }}
          children={revealedMessage}
        />
      )}
    </animated.div>
  );
};

export default MarkdownCode;
