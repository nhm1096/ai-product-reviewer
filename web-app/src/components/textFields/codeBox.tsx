import React, { useState } from "react";

interface CodeBoxProps {
  placeHolder: string;
  onTextChange: (text: string) => void;
}

const CodeBox = ({ placeHolder, onTextChange }: CodeBoxProps): JSX.Element => {
  const [inputText, setInputText] = useState('');
  
  const handleInputChange = (event: React.ChangeEvent<HTMLTextAreaElement>) => {
    setInputText(event.target.value);
    onTextChange(event.target.value);
  };

  return (
    <>
      <textarea 
        className="bg-black font-mono p-3 mt-5 w-[60%] text-md text-white rounded-lg border-2 border-black" 
        placeholder={placeHolder}
        rows={10}
        onChange={handleInputChange}
        value={inputText}
      />
    </>
  );
};

export default CodeBox;