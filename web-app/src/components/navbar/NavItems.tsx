"use client";
import {
  HomeIcon,
  Pencil1Icon,
  PersonIcon,
  PlayIcon,
  RocketIcon,
} from "@radix-ui/react-icons";
import { signIn, signOut, useSession } from "next-auth/react";
import { Dispatch, HTMLAttributes, SetStateAction } from "react";

import { classNames } from "../../lib/helpers/utils";
import BasicButton from "../buttons/basicButton";
import IconButton from "../buttons/iconButton";
import { clearGlobalStore } from "../../../helpers";
import Image from "next/image";

interface NavItemsProps extends HTMLAttributes<HTMLUListElement> {
  setOpen?: Dispatch<SetStateAction<boolean>>;
}

export const NavItems = ({
  className,
  setOpen,
  ...props
}: NavItemsProps): JSX.Element => {
  const { data: session } = useSession();
  const loggedIn = session?.user !== undefined;

  return (
    <ul
      className={classNames(
        "flex flex-row items-center gap-4 text-sm flex-1",
        className
      )}
      {...props}
    >
      <div className="flex sm:flex-1 sm:justify-end flex-col items-center justify-center sm:flex-row gap-5 sm:gap-2">
        {loggedIn ? (
          <>
            <IconButton
              icon={<HomeIcon height={25} width={25} />}
              to="/"
              setOpen={setOpen}
            />
            <IconButton
              icon={<Image src="/code-review.png" alt={"code-review"} width={25} height={25} />}
              to="/demoreview"
              setOpen={setOpen}
            />
            <IconButton
              icon={<Image src="/bug.png" alt={"bug"} width={25} height={25} />}
              to="/demofix"
              setOpen={setOpen}
            />
            <BasicButton
              text="Sign Out"
              onClick={async () => {
                clearGlobalStore();
                await signOut({ callbackUrl: "/" });
              }}
            />
          </>
        ) : (
          <>
            <IconButton
              icon={<HomeIcon height={25} width={25} />}
              to="/"
              setOpen={setOpen}
            />
            <IconButton
              icon={<Image src="/code-review.png" alt={"code-review"} width={25} height={25} />}
              to="/demoreview"
              setOpen={setOpen}
            />
            <IconButton
              icon={<Image src="/bug.png" alt={"bug"} width={25} height={25} />}
              to="/demofix"
              setOpen={setOpen}
            />
            <BasicButton
              text="Sign In"
              onClick={async () => {
                await signIn("gitlab", { callbackUrl: "/profile" });
              }}
            />
          </>
        )}
      </div>
    </ul>
  );
};
