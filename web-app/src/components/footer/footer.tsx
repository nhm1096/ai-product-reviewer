import Image from "next/image";

export const Footer = (): JSX.Element => {
  return (
    <footer className="flex flex-col items-center text-l text-white fixed bottom-0 left-0 w-full bg-black py-1">
      Copyright © 2023 MoMo
    </footer>
  );
};

Footer.displayName = "Footer";
export default Footer;
