import React, { useEffect, useState } from "react";
import "../../styles/loading.css";

const Loading = ({ text = "" }): JSX.Element => {
  const MAX_LENGTH_LOADING = 2;
  const [loadingStr, setLoadingStr] = useState("");

  useEffect(() => {
    const timer = setTimeout(() => {
      if (loadingStr.length > MAX_LENGTH_LOADING) {
        setLoadingStr("");
        return;
      }

      setLoadingStr(loadingStr + ".");
    }, 400);

    return () => {
      clearTimeout(timer);
    };
  }, [loadingStr]);

  return (
    <div
      style={{
        justifyContent: "center",
        alignItems: "center",
        flexDirection: "column",
      }}
      className="loading"
    >
      {text.length > 0 ? <h1>&nbsp; {`${text}${loadingStr}`}</h1> : null}
      <div className="loading">
        <div className="loader"></div>
      </div>
    </div>
  );
};

export default Loading;
