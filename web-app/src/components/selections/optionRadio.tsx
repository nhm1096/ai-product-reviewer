import React, { useState } from "react";

interface Option {
  value: string;
  label: string;
}

interface OptionRadioDropdownProps {
  title: string;
  options: Option[];
}

const OptionRadioDropdown: React.FC<OptionRadioDropdownProps> = ({
  title,
  options,
}) => {
  const [selectedOption, setSelectedOption] = useState<string>(
    options[0].value
  );

  const handleOptionChange = (event: React.ChangeEvent<HTMLSelectElement>) => {
    setSelectedOption(event.target.value);
  };

  return (
    <div className="p-3 mt-5 w-[60%] border border-gray-300 rounded-lg shadow-md">
      <h2 className="text-xl font-semibold mb-3">{title}</h2>
      <div className="space-y-2">
        <select
          value={selectedOption}
          onChange={handleOptionChange}
          className="block w-full p-2 mt-1 border rounded-lg"
        >
          {options.map((option) => (
            <option key={option.value} value={option.value}>
              {option.label}
            </option>
          ))}
        </select>
      </div>
      <div className="mt-3">
        <p className="text-gray-600">
          Resource Name:{" "}
          <span className="font-semibold text-blue-500">
            {selectedOption || "None"}
          </span>
        </p>
      </div>
    </div>
  );
};

export default OptionRadioDropdown;
