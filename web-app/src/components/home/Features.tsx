"use client";
import { ReactNode } from "react";
import { GiBrain, GiGearHammer, GiOpenBook } from "react-icons/gi";

import Card from "../cards/Card";

export const Features = (): JSX.Element => {
  return (
    <section className="text-center items-center justify-center gap-5">
      <div>
        <h1 className="text-4xl font-bold mb-5">Tính năng</h1>
      </div>
      <div className="flex flex-wrap gap-5 justify-center">
        <Feature
          icon={<GiBrain className="text-4xl w-full" />}
          title={"Auto code reviews"}
          description={
            "Automatically check the code of a software product against industry standards and software development principles, providing feedback and suggestions for code quality improvement."
          }
        />
        <Feature
          icon={<GiGearHammer className="text-4xl w-full" />}
          title={"Auto-suggest fixes for bugs or crashes"}
          description={
            "Analyze the overall situation and provide detailed information about bugs or crashes, automatically offering solutions ranging from basic bug fixes to optimizing the handling of complex errors."
          }
        />
        <Feature
          icon={<GiOpenBook className="text-4xl w-full" />}
          title={"Open Source"}
          description={"Free to use and contribute to. Join the community!"}
        />
      </div>
    </section>
  );
};

interface FeatureProps {
  icon?: ReactNode;
  title: string;
  description: string;
}

const Feature = ({ title, description, icon }: FeatureProps): JSX.Element => {
  return (
    <Card className="text-center p-8 max-w-xs flex flex-col gap-5 w-full">
      {icon}
      <h1 className="text-xl font-bold">{title}</h1>
      <p className="pt-2">{description}</p>
    </Card>
  );
};
