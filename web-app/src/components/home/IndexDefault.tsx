import { Button, Link } from "@radix-ui/themes";
import { motion, useScroll, useSpring, useTransform } from "framer-motion";
import React, { useRef } from "react";
import { MdNorthEast } from "react-icons/md";

import BasicButton from "../buttons/basicButton";
import { InstallationInstructions } from "../../lib/installation";
import ReactMarkdown from "react-markdown";

export function IndexDefault(): JSX.Element {
  const targetRef = useRef<HTMLDivElement | null>(null);
  const { scrollYProgress } = useScroll({
    target: targetRef,
    offset: ["start start", "end start"],
  });

  const scaleSync = useTransform(scrollYProgress, [0, 0.5], [1, 0.9]);
  const scale = useSpring(scaleSync, { mass: 0.1, stiffness: 100 });

  const position = useTransform(scrollYProgress, (pos) => {
    if (pos === 1) {
      return "relative";
    }

    return "sticky";
  });

  const videoScaleSync = useTransform(scrollYProgress, [0, 0.5], [0.9, 1]);
  const videoScale = useSpring(videoScaleSync, { mass: 0.1, stiffness: 100 });

  const opacitySync = useTransform(scrollYProgress, [0, 0.5], [1, 0]);
  const opacity = useSpring(opacitySync, { mass: 0.1, stiffness: 200 });

  return (
    <section
      ref={targetRef}
      className="relative w-full flex flex-col gap-24min-h-[768px] pt-12"
    >
      <motion.div
        style={{ scale, opacity, position }}
        className="flex flex-col gap-2 items-center text-center justify-center"
      >
        <h1 className="text-3xl sm:text-5xl font-bold max-w-lg sm:max-w-xl">
          <span className="text-pink-600">AI Product Reviewer</span>
        </h1>
        <div className="flex flex-col p-5">
          <ReactMarkdown
            className="markdown"
            children={InstallationInstructions}
          />
        </div>
      </motion.div>
    </section>
  );
}
