import type { NextApiRequest, NextApiResponse } from "next";
import { GLOBALSTORE } from "../../../../helpers";

export default function userHandler(
  req: NextApiRequest,
  res: NextApiResponse<string>
) {
  const { query, method } = req;
  const id = parseInt(query.id as string, 10);
  const name = query.name as string;

  switch (method) {
    case "GET":
      // Get data from your database
      // TODO:
      const user = GLOBALSTORE?.["user"];
      res.status(200).json(
        JSON.stringify({
          userId: user?.userId,
          name: user?.name,
          email: user?.email,
          image: user?.image
        })
      );
      break;
    default:
      res.setHeader("Allow", ["GET", "PUT"]);
      res.status(405).end(`Method ${method} Not Allowed`);
  }
}
