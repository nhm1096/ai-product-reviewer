import NextAuth, { NextAuthOptions } from "next-auth";
import GitlabProvider from "next-auth/providers/gitlab";
// import { FirestoreAdapter, initFirestore } from "@auth/firebase-adapter";
// import { cert } from "firebase-admin/app";
import { setGlobalStore } from "../../../../helpers";
// import { ENV } from "./env";

// const firestore = initFirestore({
//   credential: cert({
//     projectId: ENV.firebaseConfig.projectId,
//     clientEmail: ENV.firebaseConfig.clientEmail,
//     privateKey: ENV.firebaseConfig.privateKey,
//   }),
// });

const authOptions: NextAuthOptions = {
  providers: [
    GitlabProvider({
      authorization: {
        url: "https://gitlab.mservice.com.vn/oauth/authorize",
        params: { scope: "read_user" },
      },
      token: "https://gitlab.mservice.com.vn/oauth/token",
      userinfo: "https://gitlab.mservice.com.vn/api/v4/user",
      clientId: process.env.GITLAB_CLIENT_ID as string,
      clientSecret: process.env.GITLAB_CLIENT_SECRET as string,
    }),
  ],
  secret: process.env.NEXTAUTH_SECRET,
  // adapter: FirestoreAdapter(firestore),
  session: {
    strategy: "jwt",
  },
  callbacks: {
    // eslint-disable-next-line @typescript-eslint/require-await
    async session({ session, token }) {
      const { user } = session || {},
        { name = "", email = "", image = "" } = user || {};

      console.log("@@ session >> user: ", user);

      if (user) {
        user.userId = token.sub;
        setGlobalStore("user", user);
      }

      return session;
    },
  },
};

export default NextAuth(authOptions);
