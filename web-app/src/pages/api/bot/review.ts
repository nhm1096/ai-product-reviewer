import { ReviewBodyProps, ReviewResponseProps } from "../../../lib/types";
import { AxiosInstance } from "axios";

export const reviewCode = async (
  reviewBodyProps: ReviewBodyProps,
  axiosInstance: AxiosInstance
): Promise<ReviewResponseProps> => {
  console.log("@@ api >> call /api/bot/review: ", reviewBodyProps);
  const { data } = await axiosInstance.post(`/api/bot/review`, reviewBodyProps);
  return data as ReviewResponseProps;
};
