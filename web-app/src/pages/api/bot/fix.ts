import type { NextApiRequest, NextApiResponse } from "next";

import {
  FixBugBodyProps,
  FixBugResponseProps,
} from "../../../lib/types";
import { AxiosInstance } from "axios";

export const fixBug = async (
  fixBugBodyProps: FixBugBodyProps,
  axiosInstance: AxiosInstance
): Promise<FixBugResponseProps> => {
  console.log("@@ api >> call /api/bot/fix: ", fixBugBodyProps);
  const { data } = await axiosInstance.post(`/api/bot/fix`, fixBugBodyProps);
  return data as FixBugResponseProps;
};
