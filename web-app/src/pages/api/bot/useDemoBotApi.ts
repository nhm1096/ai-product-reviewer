import { useAxios } from "../../../lib/hooks/useAxios";
import {
  DemoApiResponse,
  FixBugBodyProps,
  ReviewBodyProps,
} from "../../../lib/types";
import { reviewCode } from "./review";
import { fixBug } from "./fix";

export const useDemoBotApi = (): DemoApiResponse => {
  const { axiosInstance } = useAxios();

  return {
    reviewCode: async (updateUserProps: ReviewBodyProps) =>
      await reviewCode(updateUserProps, axiosInstance),
    fixBug: async (getUserProps: FixBugBodyProps) =>
      await fixBug(getUserProps, axiosInstance),
  };
};
