export const BUG_JS_Override = `
function sum(a, b) {
    return a + b;
}

function sum(a, b, c) {
    return a + b + c;
}

const total = sum(1, 2);
`