"use client"
import { IndexDefault, Features } from "../components/home";

export default function Home(): JSX.Element {
  return (
    <div className="w-full h-full">
      <IndexDefault />
      <Features />
    </div>
  );
}
