"use client";
import { useSession } from "next-auth/react";
import { useEffect, useRef, useState } from "react";

import BasicButton from "../../components/buttons/basicButton";
import Loading from "../../components/loading/loading";
import CodeTextArea from "../../components/textFields/codeBox";
import MarkdownCode from "../../components/textFields/markdownCode";
import { useDemoBotApi } from "../../pages/api/bot/useDemoBotApi";
import OptionRadioDropdown from "../../components/selections/optionRadio";
import { Blockquote } from "@radix-ui/themes";

export default function Demo(): JSX.Element {
  const { status } = useSession();
  const [loading] = useState(false);
  const [isLoading, setLoading] = useState(false);
  const [passedText, setSyncedText] = useState<string>("");
  const [displayedText, setDisplayedText] = useState<string>("");

  const { reviewCode } = useDemoBotApi();

  const handleText = (text: string) => {
    setSyncedText(text);
  };

  const onClick = async () => {
    if (passedText.length <= 0) return;
    setDisplayedText("");
    setLoading(true);

    const review = await reviewCode({
      url: passedText,
    });

    const reviewData = review.data;
    console.log("@@ demoReview >> reviewData: ", reviewData);

    setLoading(false);
    setDisplayedText(reviewData);
  };

  if (status === "loading" || loading) {
    return <Loading />;
  }

  return (
    <div className="flex flex-col items-center">
      <h1 className="text-2xl sm:text-2xl font-bold max-w-lg sm:max-w-xl mt-5">
        <span className="text-pink-600">Demo Review Code</span>
      </h1>
      <h4>
        Example:
        <Blockquote>
          https://gitlab.mservice.com.vn/momo-platform/momo-app/-/merge_requests/1948
        </Blockquote>
      </h4>
      <CodeTextArea
        placeHolder={`Enter your Gitlab MergeRequests or function here...

Example: https://gitlab.mservice.com.vn/momo-platform/momo-app/-/merge_requests/1168
`}
        onTextChange={handleText}
      />
      <OptionRadioDropdown
        title={"Select Model: "}
        options={[
          { value: "momo-genai-17-4", label: "MoMo GPT3.5" },
          { value: "momo-genai-17-4-40", label: "MoMo GPT4.0" },
          { value: "local-gpt", label: "Local GPT (comming-soon)" },
        ]}
      />
      <BasicButton text="Review" onClick={onClick} styling="m-2" />
      {isLoading ? (
        <Loading text="Reviewing" />
      ) : (
        <MarkdownCode message={displayedText} isHidden={false} />
      )}
    </div>
  );
}
