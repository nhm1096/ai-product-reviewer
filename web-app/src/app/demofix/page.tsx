"use client";
import { useSession } from "next-auth/react";
import { useState } from "react";

import BasicButton from "../../components/buttons/basicButton";
import Loading from "../../components/loading/loading";
import CodeTextArea from "../../components/textFields/codeBox";
import MarkdownCode from "../../components/textFields/markdownCode";
import { gpt_crash_response } from "../../MOCK/gpt_crash_response";
import { useDemoBotApi } from "../../pages/api/bot/useDemoBotApi";
import OptionRadioDropdown from "../../components/selections/optionRadio";
import { Blockquote, Code } from "@radix-ui/themes";

export default function Demo(): JSX.Element {
  const { status } = useSession();
  const [loading] = useState(false);
  const [isLoading, setLoading] = useState(false);
  const [passedText, setSyncedText] = useState<string>("");
  const [passedTextFunction, setSyncedTextFunction] = useState<string>("");

  const [displayedText, setDisplayedText] = useState<string>("");
  const { fixBug } = useDemoBotApi();

  const handleTextStackTrace = (text: string) => {
    setSyncedText(text);
  };

  const handleTextFunction = (text: string) => {
    setSyncedTextFunction(text);
  };

  const onClick = async () => {
    setDisplayedText("");
    setLoading(true);

    const response = await fixBug({
      stacktrace: passedText,
      code: passedTextFunction,
    });

    const fixData = response.data;
    console.log("@@ demoReview >> fixData: ", fixData);

    setLoading(false);
    setDisplayedText(fixData);
  };

  if (status === "loading" || loading) {
    return <Loading />;
  }

  return (
    <div className="flex flex-col items-center">
      <h1 className="text-2xl sm:text-2xl font-bold max-w-lg sm:max-w-xl mt-5">
        <span className="text-pink-600">Demo Fix Bugs/Crashes</span>
      </h1>
      <h4>
        Example:
        <Blockquote>
          {`function sum(a, b) {
    return a + b;
}

function sum(a, b, c) {
    return a + b + c;
}

const total = sum(1, 2);`}
        </Blockquote>
      </h4>
      <CodeTextArea
        placeHolder={`Enter your crash stack traces here... (Optional)

Example: 
Fatal Exception: java.lang.RuntimeException: Illegal callback invocation from native module. This callback type only permits a single invocation from native code.
at vn.momo.platform.components.fragments.bottom_sheet.PasswordBottomSheetFragment.onPressClose(PasswordBottomSheetFragment.kt:1)
`}
        onTextChange={handleTextStackTrace}
      />
      <CodeTextArea
        placeHolder={`Enter your function or function related to the stack trace here...

Example:
fun onPressClose(result: String) {
  callback?.invoke(mapOf("type" to "onPressClose"))
  viewModel.trackingOnPressClose()
}
`}
        onTextChange={handleTextFunction}
      />

      <OptionRadioDropdown
        title={"Select Model: "}
        options={[
          { value: "momo-genai-17-4", label: "MoMo GPT3.5" },
          { value: "momo-genai-17-4-40", label: "MoMo GPT4.0" },
          { value: "local-gpt", label: "Local GPT (comming-soon)" },
        ]}
      />
      <BasicButton text="Fix" onClick={onClick} styling="m-2" />
      {isLoading ? (
        <Loading text="Fixing" />
      ) : (
        <MarkdownCode message={displayedText} isHidden={false} />
      )}
    </div>
  );
}
