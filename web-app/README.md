## Getting Started

Make a copy of the .env.example file and call it .env. Replace each of the xxx with your own NEXTAUTH_SECRET, and NEXTAUTH_URL.

To run the development server locally:

```bash
npm run dev-local
```

Open [http://localhost:3000](http://localhost:3000) with your browser to see the result.
****