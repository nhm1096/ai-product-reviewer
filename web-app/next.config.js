/** @type {import('next').NextConfig} */
const nextConfig = {
  images: {
    remotePatterns: [
      {
        protocol: "https",
        hostname: "gitlab.com",
        port: "",
        pathname: "/",
      },
      {
        protocol: "https",
        hostname: "gitlab.mservice.com.vn",
        port: "",
        pathname: "/",
      },
      {
        protocol: "https",
        hostname: "secure.gravatar.com",
        port: "",
        pathname: "/avatar/**",
      },
    ],
  },
};

module.exports = nextConfig;
