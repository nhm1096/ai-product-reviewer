export var GLOBALSTORE: any = {};


/**
 * Clears the global store by setting it to an empty object.
 *
 * @return {void} No return value.
 */
export const clearGlobalStore = (): void => {
  GLOBALSTORE = {};
};

/**
 * Sets a value in the global store using a specified key.
 *
 * @param {string} key - The key to set in the global store.
 * @param {any} value - The value to set in the global store.
 */
export const setGlobalStore = (key: string, value: any) => {
  GLOBALSTORE[key] = value;
};
