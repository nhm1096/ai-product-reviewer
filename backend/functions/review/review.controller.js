const {
  getMergeRequestInfoById,
  getPatches,
} = require("../../services/git/git.utils");
const { PostCodeReviewToGPT } = require("../../services/Utils");

exports.buildResponseReview = async (req, res) => {
  try {
    const mrLink = req?.body?.url?.trim?.();
    if (!mrLink || mrLink?.length <= 0) {
      res.status(400).send({
        message: "body.url can not be empty!",
      });
      return;
    }

    // 1. get patch diff
    const pattern = /\/merge_requests\/(\d+)$/;
    const match = mrLink.match(pattern);

    if (!Array.isArray(match)) {
      // 2.1 just review the code
      const dataResponse = await PostCodeReviewToGPT(mrLink);
      // console.log("@@XX GPT response: ", dataResponse);

      res.json({
        data: dataResponse,
      });
      return;
    }

    if (match.length == 0) {
      res.status(400).send({
        message: "MR not found!",
      });
      return;
    }

    const mergeRequestIId = match[1];

    const mergeRequestInfo = await getMergeRequestInfoById(mergeRequestIId);

    const sourceBranch = mergeRequestInfo.source_branch;
    const targetBranch = mergeRequestInfo.target_branch;

    const patchesData = await getPatches(sourceBranch, targetBranch);

    if (patchesData.length <= 0) {
      res.status(400).send({
        message: "MR diff is empty!",
      });
      return;
    }

    // 2.2 build prompt & review with GPT
    const dataResponse = await PostCodeReviewToGPT(patchesData);
    console.log("@@XX GPT response: ", dataResponse);

    res.json({
      data: dataResponse,
    });
  } catch (error) {
    if (error.response && error.response.status === 404) {
      console.error("Merge Request not found");
    } else {
      console.error(`Error: ${error.message}`);
    }
    res.status(500).send({
      error: error.message || "Internal error while getDiff.",
    });
  }
};
