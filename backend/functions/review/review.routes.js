const router = require("express").Router();
const { buildResponseReview } = require("./review.controller.js");

module.exports = (app) => {
  router.post("/review", buildResponseReview);
  app.use("/api/bot", router);
};
