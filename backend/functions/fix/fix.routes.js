const router = require("express").Router();
const { buildResponseFix, approve, reject } = require("./fix.controller.js");

module.exports = (app) => {
  router.post("/fix", buildResponseFix); // manual fix bug by providing stacktrace and code.
  router.get("/crash/:id/approve", approve); // manual fix bug by providing stacktrace and code.
  router.get("/crash/:id/reject", reject); // manual fix bug by providing stacktrace and code.
  app.use("/api/bot", router);
};
