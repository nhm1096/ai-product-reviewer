// import { postDemoReview } from "../../lib/api/demo/demo";
import { postDemoReview } from "../../../backend/services/api/gpt/PostGPT";
import {
  FixBugPrefixs,
  MessageData,
  getTopLinesOfStackTrace,
} from "./FixBugsConstant";

async function buildRequestFixBugs(fullStackTrace: string, filesCode: string) {
  // Raw crash detail => TODO: Output: Readable crash (~10-20 line of stack trace)
  let stackTrace = getTopLinesOfStackTrace(fullStackTrace, 10);
  let buildStackTrace: MessageData = {
    prefix: FixBugPrefixs.prefixStackTrace,
    message: stackTrace,
  };

  //Raw code file => TODO: Readable code (Whole function and related functions)
  //To do get function
  let buildFilesCodeMessage: MessageData = {
    prefix: FixBugPrefixs.prefixFilesCode,
    message: filesCode,
  };
  let buildFindFunctions: MessageData = {
    prefix: FixBugPrefixs.prefixFindFunctions,
    message: toMessage(buildStackTrace) + toMessage(buildFilesCodeMessage),
  };

  let response = await postDemoReview(toMessage(buildFindFunctions));

  let buildCode: MessageData = {
    prefix: FixBugPrefixs.prefixCode,
    message: response?.choices[0]?.message?.content || "",
  };

  // build requestMessage
  let buildRequestMessage: MessageData = {
    prefix: FixBugPrefixs.prefixMessage,
    message: toMessage(buildStackTrace) + toMessage(buildCode),
  };

  // retun result like example 1
  response = await postDemoReview(toMessage(buildRequestMessage));

  return response?.choices[0]?.message?.content || "";
}

function toMessage(data: MessageData) {
  return data.prefix + data.message;
}

export const FixBugsFunctions = () => {
  return {
    buildRequestFixBugs: async (fullStackTrace: string, filesCode: string) =>
      buildRequestFixBugs(fullStackTrace, filesCode),
  };
};
