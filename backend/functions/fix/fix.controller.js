const { default: Crash } = require("../../services/Models/Crash");
const { PostCodeFixBugToGPT } = require("../../services/Utils");
const { mergeRequestEvent } = require("../webhook/webhook.controller");

exports.buildResponseFix = async (req, res) => {
  try {
    const stacktrace = req?.body?.stacktrace;
    const code = req?.body?.code;

    // 1. stacktrace and code provided
    if (stacktrace?.length <= 0 && code?.length <= 0) {
      res.status(400).send({
        message: "stacktrace and code can not be empty!",
      });
      return;
    }

    // 2. build prompt & fix bug with GPT
    const dataResponse = await PostCodeFixBugToGPT(stacktrace, code);

    res.json({
      data: dataResponse,
    });
  } catch (error) {
    if (error.response && error.response.status === 404) {
      console.error("Something went wrong");
    } else {
      console.error(`Error: ${error.message}`);
    }
    res.status(500).send({
      error: error.message || "Something went wrong.",
    });
  }
};

exports.approve = async (req, res) => {
  console.log(req.params.id);
  let crash = await new Crash().get(req.params.id);
  crash.status = "2";
  crash.save();

  // 1. TODO: Process Git CLI fix patch file (git checkout, git commit, git push)

  // 2. TODO: Merge request
  // mergeRequestEvent()

  res.json({
    status: "okey",
    data: crash.toCrashData(),
  });
};

exports.reject = async (req, res) => {
  console.log(req.params.id);
  let crash = await new Crash().get(req.params.id);
  crash.status = "3";
  crash.save();

  // TODO: Close Issue

  res.json({
    status: "okey",
    data: crash.toCrashData(),
  });
};
