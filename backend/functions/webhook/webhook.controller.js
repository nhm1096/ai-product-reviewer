const { default: Crash } = require("../../services/Models/Crash");
const {
  PostCodeReviewToGPT,
  PostCodeFixBugToGPT,
} = require("../../services/Utils");
const { default: gitCli } = require("../../services/git/git-cli");
const { getPatches, commentOnPR } = require("../../services/git/git.utils");
const {
  formatStackTraceToString,
  getBlockOfCode,
  checkExist,
  saveCrashData,
  getAllFileData,
} = require("../../services/utils/CrashUtils");
const STATE_OPEN = "open";
const STATE_CLOSE = "close";

exports.mergeRequestEvent = async (req, res) => {
  try {
    // 1. get info event
    const payload = req?.body;
    const event_type = payload?.event_type;
    const object_attributes = payload?.["object_attributes"];
    const action = object_attributes?.["action"];
    const source_branch = object_attributes?.["source_branch"];
    const target_branch = object_attributes?.["target_branch"];
    const iid = object_attributes?.["iid"];

    // 2. check params valid
    if (source_branch == null || target_branch == null || iid == null) {
      res.status(500).send({
        error: "ERROR - source_branch or target_branch or id MR empty!",
      });
    }
    // 3. reslove for webhook gitlab
    res.status(200).send();

    // Handle events
    await handleMergeRequestType({
      action: action,
      event_type: event_type,
      source_branch: source_branch,
      target_branch: target_branch,
      iid: iid,
    });
  } catch (error) {
    console.error(`Error mergeRequestEvent: ${error.message}`);
  }
};

async function handleMergeRequestType({
  action,
  event_type,
  source_branch,
  target_branch,
  iid,
}) {
  switch (action) {
    case STATE_OPEN: {
      if (event_type == "issue") {
        console.info(`[WEBHOOK] Open issue not supported`);
        return;
      }
      // Hanle merge request OPEN
      console.info(
        "[WEBHOOK] mergeRequestEvent >> NEW",
        `action: ${action} - ${source_branch} --> ${target_branch} | MR_id: ${iid}`
      );
      // 5. get patches
      const patchesData = await getPatches(source_branch, target_branch);
      // 6. build prompt
      if (patchesData.length <= 0) {
        console.error(`MR diff is empty! | MR_id: ${iid}`);
        return;
      }
      // 7. send prompt with patches to GPT
      const responseGPT = await PostCodeReviewToGPT(patchesData);
      // 8. comment on MR
      await commentOnPR(iid, responseGPT);
      break;
    }
    case STATE_CLOSE: {
      if (event_type == "issue") {
        // remove firebase by iid (issueId)
        const crash = new Crash();
        crash.id = iid;
        if (crash.removeByIId()) {
          console.info(`[WEBHOOK] deleted Issue: ${iid}`);
        }
      }
      break;
    }

    // Unhandled action
    default:
      console.info(
        "[WEBHOOK] mergeRequestEvent >> unhandled",
        `action: ${action} - ${source_branch} --> ${target_branch} | MR_id: ${iid}`
      );
      break;
  }
}

exports.sentryAlertsEvent = async (req, res) => {
  try {
    // 1. get info event
    const payload = req?.body;
    const dataError = payload?.data?.error;
    const event_id = dataError?.event_id;
    const title = `${dataError?.title}: ${dataError?.culprit}`;
    const user = dataError?.user?.email;
    const webUrl = dataError?.web_url;
    const versionDist = dataError?.dist;
    const environment = dataError?.environment;
    const errorType = dataError?.type;
    const tags = dataError?.tags;

    if (errorType != "error") {
      console.log(
        `@@XX sentryAlertsEvent >> SKIP >> Unsupported errorType: ${errorType}.`
      );
      res.status(200).send();
      return;
    }

    const errorHandled = tags.find((item) => item[0] === "handled");
    if (errorHandled?.[1] != "no") {
      console.log("@@XX sentryAlertsEvent >> SKIP >> crash is handled.");
      res.status(200).send();
      return;
    }

    // exception detail
    const exception = dataError?.exception?.values?.[0];
    const exception_type = exception?.type;
    const exception_value = exception?.value;
    const exception_module = exception?.module;

    // array
    const stackTrace = exception?.stacktrace?.frames;

    // console.log("@@XX sentryAlertsEvent >> dataError: ", JSON.stringify(dataError));
    console.log("@@XX sentryAlertsEvent >> title: ", title);
    // console.log("@@XX sentryAlertsEvent >> user: ", user);
    console.log("@@XX sentryAlertsEvent >> webUrl: ", webUrl);
    console.log("@@XX sentryAlertsEvent >> versionDist: ", versionDist);
    console.log("@@XX sentryAlertsEvent >> environment: ", environment);
    // console.log("@@XX sentryAlertsEvent >> stackTrace: ", stackTrace);

    // 2 check existed crash
    const crashExisted = await checkExist(title, versionDist, environment);
    if (crashExisted) {
      console.log("@@XX sentryAlertsEvent >> SKIP >> crash existed.");
      res.status(200).send();
      return;
    }


    // 3. Format stacktrace
    const stackTraceFormatted = formatStackTraceToString(
      {
        module: exception_module,
        type: exception_type,
        value: exception_value,
      },
      stackTrace
    );

    console.log(
      "@@XX sentryAlertsEvent >> stackTraceFormatted: ",
      stackTraceFormatted
    );

    // TODO: git checkout TAG by version source-code...
    // 4. Find Path exists in stacktrace
    const pathArr = await sentryCrashHandle(stackTrace);
    const arrayPathArr = pathArr;

    if (arrayPathArr.length == 0) {
      console.log(
        "@@XX sentryAlertsEvent >> SKIP >> crash not found in source-code."
      );
      res.status(200).send();
      return;
    }

    let filePath = "";
    let functionName = "";
    let targetLineNumber = 0;
    let git_userName = "";

    for (let i = 0; i < arrayPathArr.length; i++) {
      const pathData = arrayPathArr[i];

      if (filePath.length == 0) {
        filePath = "services/git/repo/momo-app/" + pathData?.filepath;

        git_userName = await gitCli.getAuthor(pathData?.filepath);
      }
      if (functionName.length == 0) {
        functionName = pathData?.function;
      }

      if (targetLineNumber == 0) {
        targetLineNumber = pathData?.lineno;
      }
    }

    console.log("@@XX sentryAlertsEvent >> filePath: ", filePath);
    console.log("@@XX sentryAlertsEvent >> functionName: ", functionName);
    console.log(
      "@@XX sentryAlertsEvent >> targetLineNumber: ",
      targetLineNumber
    );

    // 5. Build block of code of Path
    let blockOfCode = getBlockOfCode(filePath, functionName, targetLineNumber);

    if (blockOfCode.trim().length <= 0) {
      blockOfCode = getAllFileData(filePath);
    }

    console.log("@@XX sentryAlertsEvent >> blockOfCode: ", blockOfCode);

    // 5. build prompt & fix bug with GPT
    const dataResponse = await PostCodeFixBugToGPT(
      stackTraceFormatted,
      blockOfCode
    );
    console.log("@@XX sentryAlertsEvent >> dataResponse: ", dataResponse);

    // // 6. Save FireStore => Create Issue
    const id_unique = `${exception.module}${
      !!exception.type ? `.${exception.type}` : ""
    }${
      !!exception.value ? `: ${exception.value}` : ""
    }-${versionDist}-${environment}`;
    console.log("@@XX sentryAlertsEvent >> id_unique: ", id_unique);

    const crashFunc = await saveCrashData(
      id_unique,
      {
        linkCrash: webUrl,
        title: title,
        author: git_userName,
        versionDist: versionDist,
        environment: environment,
      },
      stackTraceFormatted,
      blockOfCode,
      dataResponse
    );

    // 7. Noti Hangout (Approve/Reject)
    crashFunc.sendMessageMergeRequest();

    res.status(200).send();
  } catch (error) {
    console.error(`Error sentryAlertsEvent: ${error.message}`);
  }
};

exports.firebaseAlertsEvent = async (req, res) => {
  try {
    // 1. get info event
    const payload = req?.body;
    // const action = payload?.action;
    // const event = payload?.event;
    // const stacktrace = event?.stacktrace;
    // const frames = stacktrace?.frames;
    console.log("@@XX firebaseAlertsEvent >>: ", payload);
    // 3. reslove for webhook gitlab
    res.status(200).send();
  } catch (error) {
    console.error(`Error firebaseAlertsEvent: ${error.message}`);
  }
};

function removeDuplicatesAndKeepLast(arr) {
  const uniqueMap = {};
  const result = [];

  for (let i = arr.length - 1; i >= 0; i--) {
    const filepath = arr[i].filepath;
    if (!uniqueMap[filepath]) {
      uniqueMap[filepath] = true;
      result.unshift(arr[i]); // Add the last unique element to the beginning of the result array
    }
  }

  return result;
}

exports.sentryAlertsTest = async (req, res) => {
  // const arrayPath = await sentryCrashHandle(crash_sentry_mock);
  // console.log("@@ crash >> sentryAlertsTest: ", arrayPath);
  res.json({
    data: "[]",
  });
};

const sentryCrashHandle = async (stackTrace = []) => {
  // 1. get files crash
  try {
    const fileStacks = stackTrace
      .filter((item) => !item.data || !item.data.category)
      .map((item) => {
        const file_type = item.filename.substring(
          item.filename.lastIndexOf(".")
        );
        return {
          function: item.function,
          filename: item.filename,
          filepath:
            item.module.replace(/\./g, "/").replace(/\$\d+$/, "") +
            `${file_type}`,
          lineno: item.lineno,
          file_type: file_type,
        };
      });
    // 2. filter filepath simular
    const filteredData = removeDuplicatesAndKeepLast(fileStacks.reverse());
    // 3. get full path file from pathfile in stacktrace
    const listFilePathValid = filteredData.filter(
      (item) => item.filepath.length > 3 && item.lineno != null
    );
    // 4. getFullPath files
    let listFullFilePath = [];
    for (let index = 0; index < listFilePathValid.length; index++) {
      const item = listFilePathValid[index];
      const data = await gitCli.findFullPath(item.filepath, item.file_type);
      if (data) {
        item["filepath"] = data;
        listFullFilePath.push(item);
      }
    }

    // TODO: 5. get content file and ...
    console.log("@@XX listFullFilePath: ", listFullFilePath);
    return listFullFilePath;
  } catch (err) {
    return [];
  }
};
