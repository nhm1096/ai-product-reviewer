const router = require("express").Router();
const webhook = require("./webhook.controller.js");

module.exports = (app) => {
  // gitlab
  router.post("/merge-request", webhook.mergeRequestEvent);
  // sentry
  router.post("/sentry/alerts", webhook.sentryAlertsEvent);
  router.post("/sentry/test", webhook.sentryAlertsTest);
  // firebase
  router.post("/firebase/alerts", webhook.firebaseAlertsEvent);

  app.use("/webhook/event", router);
};
