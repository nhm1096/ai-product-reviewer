const exception_example = {
  type: "IndexOutOfBoundsException",
  value: "Index 8 out of bounds for length 5",
  module: "java.lang",
};

module.exports = exception_example;