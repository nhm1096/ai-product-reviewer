package vn.momo.platform.nativemodules.nfcreader;

import static com.facebook.react.bridge.UiThreadUtil.runOnUiThread;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.nfc.NfcAdapter;
import android.nfc.tech.IsoDep;
import android.nfc.tech.Ndef;
import android.nfc.tech.NfcA;
import android.util.Log;

import androidx.annotation.NonNull;

import com.facebook.react.bridge.ActivityEventListener;
import com.facebook.react.bridge.Arguments;
import com.facebook.react.bridge.Callback;
import com.facebook.react.bridge.LifecycleEventListener;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;
import com.facebook.react.bridge.ReadableMap;
import com.facebook.react.bridge.WritableMap;
import com.facebook.react.bridge.WritableNativeMap;

import java.util.Objects;

import vn.momo.platform.nativemodules.nfcreader.card.CardType;
import vn.momo.platform.nativemodules.nfcreader.model.CardResult;
import vn.momo.platform.nativemodules.nfcreader.model.NFCCallback;
import vn.momo.platform.nativemodules.nfcreader.model.NFCStatusEnum;
import vn.momo.platform.nativemodules.nfcreader.model.OptionParams;
import vn.momo.platform.nativemodules.nfcreader.view.NFCDialogBottomSheet;
import vn.momo.platform.nativemodules.nfcreader.view.NFCSettingDialog;

public class NFCModule extends ReactContextBaseJavaModule implements ActivityEventListener, LifecycleEventListener, NFCCallback {
    String TAG = "NFCModule";
    private Callback mNfcCallback = null;
    private NfcAdapter mNfcAdapter;
    private NFCDialogBottomSheet nfcDialogBottomSheet;
    private ReadableMap params;
    private ReactApplicationContext context;

    private String error;
    private String errorCode;
    private static final String E_SCAN_FAILED = "E_SCAN_FAILED";
    private static final String E_TIMEOUT = "E_TIMEOUT";

    NFCModule(ReactApplicationContext context) {
        super(context);
        this.context = context;
    }

    @NonNull

    @Override
    public String getName() {
        return "NFCModule";
    }

    @ReactMethod
    public void startNfc(ReadableMap opts, Callback callback) {
        context.addActivityEventListener(this);
        context.addLifecycleEventListener(this);

//        mNfcAdapter = NfcAdapter.getDefaultAdapter(context);
//
//        if(mNfcAdapter == null) return;
//        if(!mNfcAdapter.isEnabled()){
//            NFCSettingDialog dialogSetting = new NFCSettingDialog(context);
//            dialogSetting.showDialog(getCurrentActivity());
//        }
//        else {
            OptionParams.getInstance().setData(opts);
            nfcDialogBottomSheet = new NFCDialogBottomSheet(this);
            params = opts;
            runOnUiThread(new Runnable() {
                @SuppressLint("WrongConstant")
                @Override
                public void run() {
                    mNfcCallback = callback;
                    Activity activity = getCurrentActivity();
                    if (activity != null) {
                        nfcDialogBottomSheet.showDialog(activity);
                    }
                    Intent intent = new Intent(activity.getApplicationContext(), activity.getClass());
                    intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
                    PendingIntent pendingIntent = null;
                    if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.S) {
                        pendingIntent = PendingIntent.getActivity(getCurrentActivity(), 0, intent, PendingIntent.FLAG_MUTABLE);
                    } else {
                        pendingIntent = PendingIntent.getActivity(getCurrentActivity(), 0, intent, 0);
                    }
                    ReadableMap readableMap = OptionParams.getInstance().getData();
                    String type = readableMap.getString("type");
                    String technology = readableMap.getString("technology");
                    String[][] filter;
                    if (Objects.equals(type, "CIC")) {
                        filter = new String[][]{new String[]{IsoDep.class.getName()}};
                    } else {
                        switch (Objects.requireNonNull(technology)) {
                            case "IsoDep":
                                filter = new String[][]{new String[]{IsoDep.class.getName()}};
                                break;
                            case "NfcA":
                                filter = new String[][]{new String[]{NfcA.class.getName()}};
                                break;
                            default:
                                filter = new String[][]{new String[]{Ndef.class.getName()}};
                                break;
                        }

                    }
                    mNfcAdapter.enableForegroundDispatch(getCurrentActivity(), pendingIntent, null, filter);
                    nfcDialogBottomSheet.setStatusScan(NFCStatusEnum.WAITING);
                }
            });
        }
//    }

    @Override
    public void onActivityResult(Activity activity, int requestCode, int resultCode, Intent intent) {
    }

    @Override
    public void onNewIntent(Intent intent) {
        if (!NfcAdapter.ACTION_TECH_DISCOVERED.equals(intent.getAction())) return;
        nfcDialogBottomSheet.setStatusScan(NFCStatusEnum.SCANNING);

        if (mNfcAdapter != null && mNfcAdapter.isEnabled()) {
            if (Objects.equals(params.getString("type"), "CIC")) {
                NFCCardManager.readCard(CardType.CIC, intent, this);
            } else {
                NFCCardManager.readCard(CardType.DEFAULT, intent, this);
            }

        }
    }

    @Override
    public void onHostResume() {

    }

   @Override
    public void onHostPause() {
       nfcDialogBottomSheet.setStatusScan(NFCStatusEnum.DEFAULT);
    }

    @Override
    public void onHostDestroy() {

    }

    @Override
    public void onScanSuccess(CardResult cardResult) {
        nfcDialogBottomSheet.setStatusScan(NFCStatusEnum.SUCCESS);
        WritableMap resultData = new WritableNativeMap();
        WritableMap data = Arguments.createMap();
        if (cardResult != null) {
            resultData.putBoolean("status", true);
            if (cardResult.getResult().get("dg1") != null) {
                data.putString("DG1", (String) cardResult.getResult().get("dg1"));
                data.putString("DG2", (String) cardResult.getResult().get("dg2"));
                data.putString("DG13", (String) cardResult.getResult().get("dg13"));
                data.putString("DG14", (String) cardResult.getResult().get("dg14"));
                data.putString("DG15", (String) cardResult.getResult().get("dg15"));
                data.putString("COM", (String) cardResult.getResult().get("com"));
                data.putString("SOD", (String) cardResult.getResult().get("sod"));
                data.putString("mrzInfo", (String) cardResult.getResult().get("mrzInfo"));
                data.putString("photo", (String) cardResult.getResult().get("photo"));
                resultData.putMap("response", data);
            } else {
                resultData.putString("response", (String) cardResult.getResult().get("defaultResult"));
            }
        }
        mNfcCallback.invoke(resultData);
        context.removeActivityEventListener(this);
        context.removeLifecycleEventListener(this);
    }

    @Override
    public void onScanFail(String errorCode, String error) {
        mNfcAdapter.disableForegroundDispatch(getCurrentActivity());
        if(Objects.equals(errorCode, E_TIMEOUT))
        {
            nfcDialogBottomSheet.setStatusScan(NFCStatusEnum.TIMEOUT);
        }
        else if(Objects.equals(errorCode, E_SCAN_FAILED)){
            nfcDialogBottomSheet.setStatusScan(NFCStatusEnum.FAIL);
        }
        else{
            nfcDialogBottomSheet.setStatusScan(NFCStatusEnum.DISCONNECT_CARD);
        }
        if(!Objects.equals(error, "")){
            this.error = error;
        }
        this.errorCode = errorCode;
    }

    @Override
    public void onScanning(int percent) {
        nfcDialogBottomSheet.setStatusScan(NFCStatusEnum.SCANNING, percent);
    }

    @Override
    public void onRetry(Boolean isRetry) {
        if(isRetry){
            startNfc(params, mNfcCallback);
        }else{
            WritableMap resultData = new WritableNativeMap();
            WritableMap errorData = Arguments.createMap();
            errorData.putString("errorMessage", error);
            errorData.putString("errorCode", errorCode);

            resultData.putBoolean("status", false);
            resultData.putMap("error", errorData);

            mNfcCallback.invoke(resultData);
            context.removeActivityEventListener(this);
            context.removeLifecycleEventListener(this);
        }
    }
}


