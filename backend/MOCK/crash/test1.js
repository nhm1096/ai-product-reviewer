package vn.momo.platform.fragments.authstack

import android.os.Bundle
import android.os.Handler
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.platform.ComposeView
import androidx.compose.ui.platform.ViewCompositionStrategy
import androidx.fragment.app.viewModels
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.update
import vn.momo.core.di.Providers
import vn.momo.core.features.authentication.localization.localized
import vn.momo.core.features.authentication.viewmodel.AuthPhoneViewModel
import vn.momo.core.modules.analytics.constant.AppEvent
import vn.momo.core.modules.common.utilities.timer.KMMTimer
import vn.momo.core.modules.common.utilities.timer.KMMTimerOptions
import vn.momo.platform.ColdStartTracker
import vn.momo.platform.fragments.authstack.contents.AuthPhoneViewContent
import vn.momo.platform.fragments.authstack.views.AuthPhoneView
import coil.ImageLoader
import coil.request.ImageRequest
import vn.momo.core.features.authentication.getConfigImage
import vn.momo.core.features.authentication.helper.ABTestBannerSignupHelper
import vn.momo.platform.fragments.authstack.utils.AuthUtils
import vn.momo.platform.nativemodules.appcrash.AppCrashModule

class AuthPhoneFragment : AuthBaseFragment() {
    private val viewModel = AuthPhoneViewModel()
    private val content = MutableStateFlow(AuthPhoneViewContent.build())
    private val momoTracker by lazy { Providers.providesMoMoTracker() }
    private val language by lazy { Providers.providesLocalizationProvider() }

    override val tagName: String
        get() = "AuthPhone"

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        super.onCreateView(inflater, container, savedInstanceState)
        AuthUtils.reloadImage(getConfigImage(),requireContext()) ///reload image
        viewModel.setRandomABTestingBannerSignup()
        return ComposeView(requireContext()).apply {
            setViewCompositionStrategy(
                ViewCompositionStrategy.DisposeOnViewTreeLifecycleDestroyed
            )
            setContent {
                val uiState by viewModel.uiState.collectAsState()
                val contentState by content.asStateFlow().collectAsState()

                AuthPhoneView(
                    uiState = uiState,
                    vm = viewModel,
                    content = contentState,
                    language = language.get(),
                    onChangeLanguage = {
                        updateLanguage(it)
                    }
                )

                LaunchedEffect(key1 = "cold_start") {
                    ColdStartTracker.stopTrace()
                }
            }
        }
    }

    private fun updateLanguage(lang: String) {

        momoTracker.track(
            AppEvent.FeatureIntro.eventKey, mapOf(
                "action" to AppEvent.FeatureIntro.Action.click_lang,
                "button_name" to lang,
                "screen_name" to "scr_phonenumber",
                "service_name" to viewModel.uiState.value.tagnameABTestBanner
            )
        )
        language.set(lang)

        // demo crash
//        val test = ("0")
//        println(test[2])

        throw RuntimeException("AppCrash test android")

        content.update { state ->
            state.copy(
                phoneNumber = "phoneNumber".localized(),
                continueBtn = "continueBtn".localized()
            )
        }
    }

    override fun onViewBlur() {
        viewModel.clearData()
    }

    override fun onViewFocus() {
        KMMTimer(100, KMMTimerOptions.Trailing, Providers.providesDispatcherProvider().io).apply {
            setTimer {
                viewModel.onAppearEnterPhone()
                clearTimer()
            }
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        viewModel.clearData()
    }
}


