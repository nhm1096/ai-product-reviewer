package vn.momo.platform.fragments.bottom_tab_container

import android.graphics.Bitmap
import android.graphics.Color
import android.graphics.drawable.BitmapDrawable
import android.view.Menu
import android.view.MenuItem
import androidx.annotation.MainThread
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.bumptech.glide.request.target.SimpleTarget
import com.bumptech.glide.request.transition.Transition
import com.google.android.material.navigation.NavigationBarView
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.launch
import vn.momo.core.di.Providers
import vn.momo.core.di.providesUseCaseForReSyncFeature
import vn.momo.core.features.appfeature.constant.AppFeatureSignal
import vn.momo.core.features.appfeature.constant.AppFeatureType
import vn.momo.core.features.appfeature.repository.AppFeatureRepository
import vn.momo.core.modules.common.utilities.toJsonElement
import vn.momo.core.modules.storage.relational.schemes.FeatureSchema
import vn.momo.platform.R
import vn.momo.platform.composes.FloatingFeedbackDialog
import vn.momo.platform.entry.Fragments
import vn.momo.platform.fragments.BaseFragment
import vn.momo.platform.fragments.QRPaymentOfflineFragment
import vn.momo.platform.fragments.RNAppViewFragment
import vn.momo.platform.fragments.bottom_tab_container.model.BottomNavData
import vn.momo.platform.fragments.home.HomeFragment
import vn.momo.platform.fragments.paymentOffline.PaymentOfflineViewModel
import vn.momo.platform.nativemodules.navigator.model.Application
import vn.momo.platform.nativemodules.nfcreader.NFCModule
import vn.momo.platform.tracking.HomeTracking


class BottomTabContainerViewModel : ViewModel(), BottomTabDelegate,
    NavigationBarView.OnItemSelectedListener {
    private val _activeFragment = MutableStateFlow<Fragment?>(null)

    private val _fragmentMap = MutableStateFlow<MutableMap<Int, Fragment>>(mutableMapOf())

    private val _bottomNavData = MutableStateFlow<BottomNavData?>(null)

    private val _tabFeatures = MutableStateFlow<List<FeatureSchema>?>(null)

    private val appFeatureRepository: AppFeatureRepository by lazy {
        Providers.providesAppFeatureRepository()
    }

    private val reSyncFeatures by lazy {
        Providers.providesUseCaseForReSyncFeature()
    }

    private val appFeatureSignalSubject by lazy {
        Providers.providesAppFeatureSignalSubject()
    }

    private val observableStorageSignalSubject by lazy {
        Providers.providesObservableStorageSignalSubject()
    }

    private val momoLogger by lazy { Providers.providesMoMoLogger() }

    private val scope = CoroutineScope(Job() + Providers.providesDispatcherProvider().io)

    private fun getHomeTabFeatures(): List<FeatureSchema>? {
        return appFeatureRepository.queryFeaturesWithTypes(listOf(AppFeatureType.of("HOME_TABBAR")))
    }

    fun initBottomNavData(
        indicator: BottomTabContainerIndicator,
        fragment: Fragment,
        childFragmentManager: FragmentManager
    ) {
        _bottomNavData.value = BottomNavData(
            indicator,
            fragment,
            childFragmentManager
        )
    }

    fun initAndObserveTabFeatures() {
        // load default home fragment
        loadFragment(BottomTab.HOME.index)
        initTabFeatures()

        viewModelScope.launch(Dispatchers.IO) {
            appFeatureSignalSubject
                .source()
                .collect { signal ->
                    if (signal is AppFeatureSignal.OnCompleteFetchingNew) {
                        initTabFeatures()
                    }
                }
        }

    }

    private fun initTabFeatures() {
        val homeTabFeatures = getHomeTabFeatures()
        val filteredHomeTabFeatures = homeTabFeatures?.filter { feature -> feature.ID != "dev_tool" }
        if (_tabFeatures.value?.size != filteredHomeTabFeatures?.size && filteredHomeTabFeatures?.isNotEmpty() == true) {
            _tabFeatures.value = filteredHomeTabFeatures
            initMenuItems()

        } else {
            momoLogger.w("features is empty!", null, TAG)
            scope.launch {
                // sync if there aren't any tab home features left
                reSyncFeatures.implement()
            }
        }
    }

    private fun getTabFeatures() = _tabFeatures.value

    fun getBottomNavData() = _bottomNavData.value

    private fun setActiveFragment(fragment: Fragment) {
        _activeFragment.value = fragment
    }

    fun getActiveFragment() = _activeFragment.value

    private fun getFragmentMap() = _fragmentMap.value

    @Synchronized
    @MainThread
    override fun gotoTab(tab: BottomTab) {
        throw RuntimeException("test bottomtab")
        getBottomNavData()?.bottomNav?.let { bottomNav ->
            try {
                bottomNav.selectedItemId = tab.index
            } catch (e: Exception) {
                momoLogger.e("error: $e", e, TAG)
            }
        }
    }

    @Synchronized
    @MainThread
    private fun loadFragment(index: Int) {
        getBottomNavData()?.childFragmentManager?.let { childFragmentManager ->
            if (childFragmentManager.isDestroyed) return@let

            val transaction = childFragmentManager.beginTransaction()

            getActiveFragment()?.let {
                transaction.hide(it)
            }

            val currentFragment = getFragmentMap()[index]
            if (currentFragment != null) {
                transaction.show(currentFragment)
                setActiveFragment(currentFragment)
            } else {
                // case no current fragment active
                val newFragment = getTabFeatures()?.get(index)?.let { feature ->
                    val application = Application.initWithFeature(feature.toMap())
                    when (application.container) {
                        Fragments.HomeContainer -> HomeFragment()
                        Fragments.ReactNativeAppContainer -> {
                            RNAppViewFragment.initWithApplication(application)
                        }

                        else -> {
                            object : BaseFragment() {
                                override fun getResourceId() = null
                            }
                        }
                    }
                } ?: HomeFragment()

                transaction.add(R.id.container, newFragment)
                _fragmentMap.value[index] = newFragment
                setActiveFragment(newFragment)
            }

            transaction.commit()
        }
    }

    private fun mappingDefaultIcon(index: Int): Int {
        return when (index) {
            1 -> R.drawable.momomain_gift
            2 -> R.drawable.momomain_history
            3 -> R.drawable.momomain_chat
            4 -> R.drawable.momomain_wallet
            else -> R.drawable.momomain_momo
        }
    }

    @MainThread
    private fun loadIcon(item: MenuItem, url: String?) {
        item.setIcon(mappingDefaultIcon(item.itemId))
        getBottomNavData()?.fragment?.let { fragment ->
            if (fragment.isAdded && fragment.activity != null && fragment.isResumed) {
                Glide.with(fragment.requireActivity())
                    .asBitmap()
                    .load(url)
                    .apply(RequestOptions.circleCropTransform())
                    .into(object : SimpleTarget<Bitmap?>() {
                        override fun onResourceReady(
                            resource: Bitmap,
                            transition: Transition<in Bitmap?>?
                        ) {
                            if (fragment.isAdded && fragment.activity != null && fragment.isResumed) {
                                item.icon = BitmapDrawable(fragment.resources, resource)
                            }
                        }
                    })
            }
        }
    }

    @Synchronized
    @MainThread
    private fun initMenuItems() {
        viewModelScope.launch {
            getBottomNavData()?.bottomNav?.let { bottomNav ->
                var count = 0

                bottomNav.menu.clear()
                getTabFeatures()?.forEach { feature ->
                    if (count == 0) {
                        CurrentTabFeatureObservable.getInstance()
                            .setCurrentTabFeature(feature)
                    }
                    if (count >= MAX_TAB_ITEMS) {
                        return@forEach
                    }
                    val item = bottomNav.menu.add(Menu.NONE, count, Menu.NONE, feature.name)
                    loadIcon(item, feature.icon)
                    viewModelScope.launch {
                        observableStorageSignalSubject
                            .sourceForKey((observableStorageSignalSubject.STORAGE_PREFIX + "badgeObserver." + feature.code).lowercase())
                            .collect {
                                getBottomNavData()?.bottomNav?.let { bottomNav ->
                                    val badge = bottomNav.getOrCreateBadge(item.itemId)
                                    val isReddot = it.data == "0"

                                    if (isReddot) {
                                        badge.clearNumber()
                                        badge.isVisible = true
                                        badge.backgroundColor = Color.RED
                                        badge.verticalOffset = 8
                                    } else {
                                        val number = it.data.toIntOrNull()
                                        if (number != null && number > 0) {
                                            badge.number = number
                                            badge.isVisible = true
                                            badge.backgroundColor = Color.RED
                                            badge.verticalOffset = 8
                                        } else {
                                            badge.isVisible = false
                                        }
                                    } 
                                }
                            }
                    }
                    count++
                }
                bottomNav.invalidate()
                bottomNav.setOnItemSelectedListener(this@BottomTabContainerViewModel)
            }
        }
    }

    fun simpleCrash(){
        val map = emptyMap<String, String>()
        val getIndex = map.get("index") as Int
    }

    fun concurrentModificationExceptionCrash(){
        val numbers = mutableListOf(1, 2, 3, 4, 5)

        for (number in numbers) {
            if (number == 3) {
                numbers.remove(number) // Attempt to modify the list while iterating
            }
        }
    }

    fun uiCrash(){
        QRPaymentOfflineFragment(PaymentOfflineViewModel(context = this), null)
    }

    private fun PaymentOfflineViewModel(context: BottomTabContainerViewModel): PaymentOfflineViewModel {
        return TODO("Provide the return value")
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
//        throw RuntimeException("tset resr")
        print("luunv ${item.itemId}")
        when(item.itemId){
            1 -> simpleCrash()
            2 -> concurrentModificationExceptionCrash()
            3 -> uiCrash()
            4 ->{
               val feature =  getTabFeatures()!!.get(item.itemId)
                    HomeTracking.homeSnackBarViewed(feature.code ?: "")
                    getBottomNavData()?.bottomNav?.onItemSelected(item)
                    CurrentTabFeatureObservable.getInstance().apply {
                        setCurrentTabFeature(feature)
                    }
                    loadFragment(8)
                    initMenuItems()
                    initTabFeatures()
            }
        }
        if (FloatingFeedbackDialog.currentDialog?.isAdded == true) {
            FloatingFeedbackDialog.currentDialog?.dismiss()
        }

        val index = item.itemId
        getTabFeatures()?.get(index)?.let { feature ->
            getBottomNavData()?.bottomNav?.onItemSelected(item)
            CurrentTabFeatureObservable.getInstance().apply {
                sendEventClickTab(feature, index)
                setCurrentTabFeature(feature)
            }
            loadFragment(index)
        }
        return true
    }

    companion object {
        const val MAX_TAB_ITEMS = 5
        const val TAG = "[BOTTOM-TAB]"
    }

}
