import OpenAI from "openai";

const openai = new OpenAI({
  apiKey: "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",
  dangerouslyAllowBrowser: true,
});

export const PostGPTOpenAI = async (
  code: string
): Promise<OpenAI.Chat.Completions.ChatCompletion | null> => {
  try {
    code = code.replace("“", '"');

    const completion = await openai.chat.completions.create({
      messages: [{ role: "user", content: code }],
      model: "gpt-3.5-turbo",
    });

    return completion;
  } catch (error) {
    console.log(error);
    return null;
  }
};
