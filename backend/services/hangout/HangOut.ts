import { ENV } from "../../env";

const fetch = require("node-fetch");

export type Button = {
  text: string;
  link: string;
};

export type MessageWithButtons = {
  title: string;
  subtitle: string;
  buttons: [Button];
};

class HangOut {
  private static instance: HangOut | null = null;
  private constructor() {
    // Private constructor to prevent instantiation from outside
  }

  static getInstance(): HangOut {
    if (this.instance != null) {
      this.instance = new HangOut();
    }
    return this.instance || new HangOut();
  }

  sendMessage(text: string) {
    const message = {
      text,
    };
    this.fetchPost(message);
  }

  sendMessageWithButtons(data: MessageWithButtons) {
    var message: any = {
      cardsV2: [
        {
          card: {
            header: {
              title: data.title,
              //   subtitle: data.subtitle,
            },
            sections: [
              {
                widgets: [
                  {
                    textParagraph: { text: data.subtitle },
                  },
                  {
                    buttonList: {
                      buttons: [],
                    },
                  },
                ],
              },
            ],
          },
        },
      ],
    };
    data.buttons.forEach((element) => {
      message.cardsV2[0].card.sections[0].widgets[1].buttonList.buttons.push({
        text: element.text,
        onClick: {
          openLink: {
            url: element.link,
          },
        },
      });
    });

    console.log(message);

    this.fetchPost(message);
  }

  private fetchPost(message: any) {
    // Send the message using fetch
    fetch(ENV.YOUR_HANGOUTS_CHAT_WEBHOOK_URL, {
      method: "POST",
      headers: {
        "Content-Type": "application/json; charset=UTF-8",
      },
      body: JSON.stringify(message),
    })
      .then((response: any) => {
        if (response.ok) {
          console.log("Message sent successfully");
        } else {
          console.error(
            "Failed to send message:",
            response.status,
            response.statusText
          );
        }
      })
      .catch((error: any) => {
        console.error("Error:", error.message);
      });
  }
}

export default HangOut.getInstance();
