import { sleep } from "openai/core";
import Crash from "../Models/Crash";
import HangOut, { MessageWithButtons } from "./HangOut";
import { ENV } from "../../env";

export interface CrashNotification {
  noti: (data: Crash) => void;
}

class CrashNotificationHangOut implements CrashNotification {
  private static instance: CrashNotificationHangOut | null = null;
  private constructor() {
    // Private constructor to prevent instantiation from outside
  }

  static getInstance(): CrashNotificationHangOut {
    if (this.instance != null) {
      this.instance = new CrashNotificationHangOut();
    }
    return this.instance || new CrashNotificationHangOut();
  }

  noti(data: Crash) {
    // HangOut.sendMessage(`Crash ID: ${data.id}\nTitle: ${data.title}\nEnvironment: ${data.environment}\nVersion: ${data.versionDist}\nAuthor: ${data.author}\nLink Crash: ${data.linkCrash}\nLink Issue: ${data.getIssueURL()}`)
    sleep(1000);
    let message: any = {
      title: `[Issue ${data.issueId}] ${data.title}`,
      subtitle: `<b>Link issue:</b> <a href=\"${data.getIssueURL()}\">https://gitlab.mservice.com.vn/momo-platform</a>\n<b>Link crash:</b> <a href=\"${
        data.linkCrash
      }\">https://sentry.io/organizations/the-ai-gang</a>\n<b>Author:</b> ${
        data.author
      }\n<b>Version:</b> ${data.versionDist}\n<b>Environment:</b> ${data.environment}\n`,
      buttons: [
        {
          text: "View Issue",
          link: `${data.getIssueURL()}`,
        },
        // {
        //   text: "Close Issue",
        //   link: `${ENV.HOSTNAME}/api/bot/crash/${data.id}/reject`,
        // },
        //   {
        //     "text": "Approve",
        //     "link": `${ENV.HOSTNAME}/api/bot/crash/${data.id}/approve`,
        //   },
        //   {
        //     "text": "Reject",
        //     "link": `${ENV.HOSTNAME}/api/bot/crash/${data.id}/reject`,
        //   }
      ],
    };
    HangOut.sendMessageWithButtons(message);
  }
}

export default CrashNotificationHangOut.getInstance();
