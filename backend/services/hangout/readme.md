




let data = {
  title: "Crash ID: 0IIhDCqWIkrz69SOiOWs",
  subtitle: "Actions with this crash:",
  buttons: [
    {
      "text": "Approve",
      "link": "https://demo.com",
    },
    {
      "text": "Reject",
      "link": "https://demo.com",
    }
  ]
}


HangOut.sendMessage(`
Crash ID: 0IIhDCqWIkrz69SOiOWs
linkCrash: https://console.firebase.google.com/u/0/project/project-5400504384186300846/crashlytics/app/android:com.mservice.momotransfer/issues/59eae5f4ed21b157f8bbf5b085e9aa54?time=last-seven-days&types=crash&versions=4.1.4%20(41042)&sessionEventKey=6507BF410264000123D9F4361752A272_1858443607214807563
`)
HangOut.sendMessageWithButtons(data)