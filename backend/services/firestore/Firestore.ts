// Import the functions you need from the SDKs you need
import { FirebaseApp, initializeApp } from "firebase/app";
import { ENV } from "../../env";
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

class FirestoreSingleton {
    private static instance: FirebaseApp | null = null;

    private constructor() {
        // Private constructor to prevent instantiation from outside
    }

    static getInstance(): FirebaseApp {
        if (this.instance != null) {
            this.instance = initializeApp(ENV.firebaseConfig);
        }
        return this.instance || initializeApp(ENV.firebaseConfig);
    }
}


export default FirestoreSingleton