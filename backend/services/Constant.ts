export type GptOpenAIResponse = {
  id: string;
  object: string;
  created: number;
  model: string;
  choices: [object];
  usage: object;
};
