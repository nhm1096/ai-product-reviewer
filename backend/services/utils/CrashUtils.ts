import Crash from "../Models/Crash";

const fs = require("fs");
const MAX_STACK_TRACE_LENGTH = 20;
const MAX_RELATED_CODE_LENGTH = 30;

/**
 * Formats a stack trace and returns it as a string.
 *
 * @param {any} exception - The exception object.
 * @param {Array<any>} stacktrace - The stack trace array.
 * @return {string} The formatted stack trace as a string.
 */
export function formatStackTraceToString(
  exception: any,
  stacktrace: Array<any>
) {
  if (!Array.isArray(stacktrace)) return "";

  const stackTraceArr = stacktrace.reverse();
  if (stackTraceArr.length == 0) return "";

  let stackTraceFormatted = `${exception.module}${
    !!exception.type ? `.${exception.type}` : ""
  }${!!exception.value ? `: ${exception.value}` : ""}\n`;

  for (let i = 0; i < stackTraceArr.length; i++) {
    if (i > MAX_STACK_TRACE_LENGTH) continue;

    const {
      module = "",
      filename = "",
      lineno = 0,
      data = {},
    } = stackTraceArr[i];

    const functionName = stackTraceArr[i]?.function ?? "";
    stackTraceFormatted +=
      "at " + module + "." + functionName + `(${filename}:${lineno})` + "\n";
  }

  return stackTraceFormatted;
}

export function getAllFileData(filePath: string) {
  try {
    const data = fs.readFileSync(filePath, "utf8");
    return data;
  } catch (err) {
    console.error("Error reading file:", err);
    return null;
  }
}

/**
 * Retrieves a block of code from a specified file, starting from the line of a given function and including all lines until the closing brace of that function.
 *
 * @param {string} filePath - The path of the file to read.
 * @param {string} functionName - The name of the function to search for.
 * @param {number} targetLineNumber - The line number where the function starts.
 * @return {string} The code block as a string.
 */
export function getBlockOfCode(
  filePath: string,
  functionName: string,
  targetLineNumber: number
) {
  let codeBlock = "";
  let insideFunction = false;
  let nearestFunctionName = "";
  let nearestFunctionStartLine = 0;
  let insideFunctionDepth = 0;

  try {
    const data = fs.readFileSync(filePath, "utf8");
    const lines = data.split("\n");

    for (const line of lines) {
      if (line.includes(`${functionName}(`)) {
        insideFunction = true;
        insideFunctionDepth = 0;
        nearestFunctionName = functionName;
        nearestFunctionStartLine = lines.indexOf(line) + 1;
      }

      if (insideFunction) {
        codeBlock += line + "\n";

        if (line.includes("{")) {
          insideFunctionDepth++;
        }
        if (line.includes("}")) {
          insideFunctionDepth--;

          if (insideFunctionDepth === 0) {
            insideFunction = false;

            // Check if this function is closer to the target line
            if (
              Math.abs(targetLineNumber - nearestFunctionStartLine) <
              Math.abs(targetLineNumber - (lines.indexOf(line) + 1))
            ) {
              break;
            }
          }
        }
      }

      if (
        lines.indexOf(line) + 1 >=
        targetLineNumber + MAX_RELATED_CODE_LENGTH
      ) {
        break;
      }
    }

    return codeBlock.trim();
  } catch (err) {
    console.error("Error reading file:", err);
    return null;
  }
}

export type CrashData = {
  linkCrash: string;
  title: string;
  author: string;
  versionDist: string;
  environment: string;
};

export async function checkExist(
  title: string,
  version: string,
  environment: string
) {
  let crash = new Crash();
  crash.title = title;
  crash.versionDist = version;
  crash.environment = environment;
  await crash.getExist();
  console.log("@@ crash >> checkExist - id: ", crash.id);
  if (crash.id != "") {
    return true;
  } else {
    return false;
  }
}

export async function saveCrashData(
  event_id: string,
  crashData: CrashData,
  stacktrace: string,
  code: string,
  dataGPT: string
) {
  let crash = new Crash();
  crash.id = event_id;
  crash.stackTrace = stacktrace;
  crash.code = code;
  crash.linkCrash = crashData.linkCrash;
  crash.title = crashData.title;
  crash.author = crashData.author;
  crash.versionDist = crashData.versionDist;
  crash.environment = crashData.environment;
  crash.resolvedCode = dataGPT;
  await crash.createIssue();
  await crash.save();
  return crash;
}
