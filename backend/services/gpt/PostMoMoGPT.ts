import OpenAI from "openai";

const fetch = require("node-fetch");

const azure = {
  apiKey: "cc5c2a21482c4dfa895806dfebad9934",
  version: "2023-05-15",
  resourceName: "momo-genai-17-4",
  gpt35: "gpt-35",
  gpt4: "gpt-4",
};

const endpoint = `https://${azure.resourceName}.openai.azure.com/openai/deployments/${azure.gpt35}/chat/completions?api-version=${azure.version}`;

export const PostMoMoGPT = async (
  code: string
): Promise<OpenAI.Chat.Completions.ChatCompletion | null> => {
  try {
    code = code.replace("“", '"');
    const payload = {
      messages: [{ role: "user", content: code }],
    };

    const options = {
      contentType: "application/json",
      muteHttpExceptions: true,
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        "api-key": azure.apiKey,
      },
      body: JSON.stringify(payload),
    };

    const response = await fetch(endpoint, options);

    return (await response.json()) as OpenAI.Chat.Completions.ChatCompletion;
  } catch (error) {
    console.log(error);

    return null;
  }
};
