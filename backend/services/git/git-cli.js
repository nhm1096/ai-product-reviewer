const utill = require("util");

const simpleGit = require("simple-git");
const fs = require("fs");

class GitCLI {
  constructor() {
    this.repoName = "momo-app";
    this.localPath = `${process.cwd()}/services/git/repo`;
    this.baseDir = `${this.localPath}/${this.repoName}`;
    this.repoPath = "https://gitlab.mservice.com.vn/momo-platform/momo-app.git";
    console.log(this.baseDir);

    // Setup gitCLI
    const options = {
      baseDir: this.baseDir,
      binary: "git",
      maxConcurrentProcesses: 6,
      trimmed: false,
    };
    this.gitCLI = simpleGit(options);
    this.fetchNew();
  }

  // TODO: not work (clone code manual -> yarn git:clone)
  async clone() {
    try {
      // Clone repo if needed
      if (fs.existsSync(this.baseDir)) {
        console.log(`[GIT-CLI] The repo ${this.repoName} exists`);
      } else {
        console.log("[GIT-CLI] The directory does NOT exist -> Cloning...");
        const result = await simpleGit().clone(
          this.repoPath,
          this.baseDir,
          null
        );
        console.log("@@XX result: ", result);
      }
    } catch (error) {
      console.log("[GIT-CLI] ERROR: ", error);
    }
  }

  async fetchNew() {
    await this.gitCLI
      .fetch()
      .then((update, error) => {
        if (error) {
          throw error;
        } else {
          console.log(`[GIT-CLI] fetch repo ${this.repoName} successful`);
        }
      })
      .catch((error) => {
        console.error("[GIT-CLI] Error:", error);
      });
  }

  async pullNew() {
    await this.gitCLI
      .pull()
      .then((update, error) => {
        if (error) {
          throw error;
        } else {
          console.log(`[GIT-CLI] pull repo ${this.repoName} successful`);
        }
      })
      .catch((error) => {
        console.error("[GIT-CLI] Error:", error);
      });
  }

  async findFullPath(inputPath, file_type) {
    const stdout = await this.getAllPathFile(file_type);
    const allPaths = stdout;

    const regexPattern = `.*${inputPath}.*`; // Create a string with the parameter
    const phoneRegex = new RegExp(regexPattern, "g");
    const matches = allPaths.match(phoneRegex);

    return matches?.[0] ?? null;
  }

  async getAllPathFile(file_type) {
    let childProcess;
    try {
      // get fullPaths on repo
      childProcess = utill.promisify(require("child_process").exec);

      const promise = childProcess(
        `cd ${process.cwd()}/services/git/repo/${
          this.repoName
        } && git ls-files \"*${file_type}\"`
      );
      const { stdout, stderr } = await promise;
      promise.child.kill();
      return stdout;
    } catch (error) {
      console.error("Error:", error);
    } finally {
      // To close the child process, check if it exists and kill it
      if (childProcess && childProcess.child) {
        childProcess.child.kill();
      }
    }
  }

  async getAuthor(filePath = `index.js`) {
    let childProcess;
    try {
      // get fullPaths on repo
      childProcess = utill.promisify(require("child_process").exec);

      const promise = childProcess(
        `cd ${this.localPath}/${this.repoName} && git log -n 1 --pretty=format:"%ae" -- ${filePath} | cut -d "@" -f 1`
      );

      const { stdout, stderr } = await promise;
      promise.child.kill();
      console.log("@@RR author: ", stdout.trim());
      return stdout.trim();
    } catch (error) {
      console.error("Error:", error);
    } finally {
      // To close the child process, check if it exists and kill it
      if (childProcess && childProcess.child) {
        childProcess.child.kill();
      }
    }
  }
}
export default new GitCLI();
