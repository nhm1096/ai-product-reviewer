const { Gitlab } = require("@gitbeaker/rest");
const {
  GITLAB_URL,
  GITLAB_TOKEN,
  PROJECT_ID,
  MADE_BY_INFO,
  GITLAB_TOKEN_BOT,
} = require("./constant");
const { default: gitCli } = require("./git-cli");

const gitlab = new Gitlab({
  host: GITLAB_URL,
  token: GITLAB_TOKEN,
});
const gitlab_bot = new Gitlab({
  host: GITLAB_URL,
  token: GITLAB_TOKEN_BOT,
});

exports.getMergeRequestInfoById = async (mergeRequestIId) => {
  const info = await gitlab.MergeRequests.show(PROJECT_ID, mergeRequestIId);
  return info;
};

exports.getPatches = async (sourceBranch, targetBranch) => {
  console.debug(`@@XX MR compare: ${sourceBranch}, ${targetBranch}`);

  await gitCli.fetchNew();
  await gitCli.pullNew();

  const diff = await gitlab.Repositories.compare(
    PROJECT_ID,
    `${targetBranch}`,
    `${sourceBranch}`,
    {
      straight: false,
      unidiff: true,
    }
  );

  const patches = diff.diffs.map((fileDiff) => fileDiff.diff);
  const patchesData = patches.join();
  //   console.debug("@@XX patchesData: ", patchesData);
  return patchesData;
};

exports.commentOnPR = async (
  mergeRequestIId,
  comment,
  signOff = "@AI-Gang-Bot-Reviewer"
) => {
  const notes = await gitlab_bot.MergeRequestNotes.all(
    PROJECT_ID,
    mergeRequestIId
  );
  const botComment = notes.find((note) => note.body.includes(signOff));
  const botCommentBody = `${comment}\n\n---\n\n${signOff} | ${MADE_BY_INFO}`;
  if (botComment) {
    await gitlab_bot.MergeRequestNotes.edit(
      PROJECT_ID,
      mergeRequestIId,
      botComment.id,
      { body: botCommentBody }
    );
  } else {
    await gitlab_bot.MergeRequestNotes.create(
      PROJECT_ID,
      mergeRequestIId,
      botCommentBody
    );
  }
  console.log(`@@XX comment MR: ${mergeRequestIId} success!`);
};

exports.createIssues = async (title, description, assigneeId) => {
  try {
    return await gitlab_bot.Issues.create(PROJECT_ID, title, {
      labels: "crash-label",
      description: description,
      assigneeId
    });
    // console.log("@@XX createIssues>>: ", result);
  } catch (error) {
    console.log(error);
  }
};

exports.findAssigneeID = async (username) => {
  let users = await gitlab.Users.all({ username: username })
  console.log("@@ git >> users: ", users)
  return users[0]?.id || 0
};

exports.closeIssues = async (issueId) => {
  try {
    return await gitlab_bot.Issues.edit(PROJECT_ID, issueId, {
      stateEvent: "close",
    });
  } catch (error) {
    console.log(error);
  }
};

exports.createMergeRequest = async() => {
  const sourceBranch = 'evitan/master'; // Replace with your source branch
  const targetBranch = 'evitan/the_ai_gang'; // Replace with your target branch
  const title = 'Fix issue: ';
  const description = 'Description of the merge request';

  let mergeRequest = await gitlab.MergeRequests.create(PROJECT_ID, sourceBranch, targetBranch, title, {
    description,
  })

  
  console.debug('DEBUG_DATDO mergeRequest', mergeRequest);
  

  return mergeRequest.web_url
}
