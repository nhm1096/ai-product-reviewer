

const crash = new Crash();
const crashData =  {
  id:  "0IIhDCqWIkrz69SOiOWs",
  linkCrash:  "https://console.firebase.google.com/",
  stackTrace:  "...",
  code:  "...",
  resolvedCode:  "...",
  status:  "..",
}
crash.fromCrashData(crashData)

// Save to FireStore
crash.save()
