import {
  getFirestore,
  collection,
  addDoc,
  Firestore,
  getDocs,
  getDoc,
  doc,
  updateDoc,
  query,
  where,
  deleteDoc,
} from "firebase/firestore";
import FirestoreSingleton from "../firestore/Firestore";
import { FirebaseApp } from "firebase/app";
import CrashNotificationHangOut, {
  CrashNotification,
} from "../hangout/CrashNotificationHangOut";
import { createIssues, findAssigneeID } from "../git/git.utils";
import { FixBugPrefixs } from "../../functions/fix/FixBugsConstant";

export interface CrashData {
  id: string;
  title: string;
  author: string;
  linkCrash: string;
  stackTrace: string;
  code: string;
  resolvedCode: string;
  status: string;
  issueId: string;
  versionDist: string;
  environment: string;
}

class Crash {
  public id: string = "";
  public title: string = "";
  public author: string = "";
  public linkCrash: string = "";
  public stackTrace: string = "";
  public code: string = "";
  public resolvedCode: string = "";
  public status: string = "";
  public issueId: string = "";
  public versionDist: string = "";
  public environment: string = "";

  private app: FirebaseApp;
  private db: Firestore;
  private collectionName: string;
  private notifers: CrashNotification[] = [CrashNotificationHangOut];

  constructor() {
    this.app = FirestoreSingleton.getInstance();
    this.db = getFirestore(this.app);
    this.collectionName = "crashes";
  }

  async save() {
    try {
      const dataCollection = collection(this.db, this.collectionName);
      const docRef = await addDoc(dataCollection, this.toCrashData());

      this.id = docRef.id;
      this.update();
      console.log("Person added with ID: ", docRef.id);
    } catch (error) {
      console.error("Error adding person: ", error);
    }
  }

  async update() {
    console.log("Updating crash with id: ", this.id);
    const crashDocRef = doc(this.db, this.collectionName, this.id);
    await updateDoc(crashDocRef, this.toCrashData());
  }

  // async update(crashData: CrashDat2) {
  //     console.log("Updating crash with id: ", crashData.id);
  //     const crashDocRef = doc(this.db, this.collectionName, crashData.id);
  //     await updateDoc(crashDocRef, crashData);
  // }

  async get(id: string) {
    try {
      const crashDocRef = doc(this.db, this.collectionName, id);
      const docSnapshot = await getDoc(crashDocRef);

      if (docSnapshot.exists()) {
        const crashData = docSnapshot.data() as CrashData;
        this.fromCrashData(crashData);
        return this;
      } else {
        console.log("No such document!");
        return null;
      }
    } catch (error) {
      console.error("Error fetching crash data: ", error);
      return [];
    }
  }

  fromCrashData(crashData: CrashData) {
    this.id = crashData.id;
    this.title = crashData.title;
    this.author = crashData.author;
    this.linkCrash = crashData.linkCrash;
    this.stackTrace = crashData.stackTrace;
    this.code = crashData.code;
    this.resolvedCode = crashData.resolvedCode;
    this.status = crashData.status;
    this.issueId = crashData.issueId;
    this.versionDist = crashData.versionDist;
    this.environment = crashData.environment;
  }

  toCrashData() {
    return {
      id: this.id,
      title: this.title,
      author: this.author,
      linkCrash: this.linkCrash,
      stackTrace: this.stackTrace,
      code: this.code,
      resolvedCode: this.resolvedCode,
      status: this.status,
      issueId: this.issueId,
      versionDist: this.versionDist,
      environment: this.environment,
    };
  }

  sendMessageMergeRequest() {
    this.notifers.forEach((notifer) => {
      notifer.noti(this);
    });
  }

  getIssueURL() {
    return (
      "https://gitlab.mservice.com.vn/momo-platform/momo-app/-/issues/" +
      this.issueId
    );
  }

  async createIssue() {
    let assigneeId = await findAssigneeID(this.author);
    let issue: any = await createIssues(
      `[Issue] ${this.title}`,
      this.formatDescription(),
      assigneeId
    );
    console.log(issue.iid);
    this.issueId = issue.iid || "test issueId";
  }

  private formatDescription() {
    return `${FixBugPrefixs.prefixStackTrace}\n\`\`\`\n${this.stackTrace}\n\`\`\`\n\n${this.resolvedCode}`;
  }

  async getExist() {
    try {
      const dataCollection = collection(this.db, this.collectionName);
      const q = query(
        dataCollection,
        where("title", "==", this.title),
        where("versionDist", "==", this.versionDist),
        where("environment", "==", this.environment)
      );

      const querySnapshot = await getDocs(q);
      // console.log(querySnapshot.size)
      querySnapshot.forEach((doc) => {
        return this.fromCrashData(doc.data() as any);
      });
    } catch (error) {
      console.error("Error getting documents: ", error);
      return false;
    }
  }

  async removeByIId() {
    try {
      const dataCollection = collection(this.db, this.collectionName);
      const q = query(dataCollection, where("issueId", "==", this.id));
      const querySnapshot = await getDocs(q);
      // console.log(querySnapshot.size)
      querySnapshot.forEach(async (docRef) => {
        if (docRef.exists()) {
          const crashDocRef = doc(this.db, this.collectionName, docRef.id);
          await deleteDoc(crashDocRef);
          return true;
        }
        return false;
      });
    } catch (error) {
      console.error("Error getting documents: ", error);
      return false;
    }
  }
}

export default Crash;

type CrashDat2 = {
  [key: string]: any;
};
