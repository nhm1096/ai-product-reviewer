const express = require("express");
const morgan = require("morgan");
const { default: Crash } = require("./services/Models/Crash");

const app = express();

app.use(morgan("tiny")); // logger

const ENV = "development";
const DOMAIN = ENV === "development" ? "10.40.114.92" : "10.40.114.92";
const PORT = 3001;

app.use(function (req, res, next) {
  res.setHeader("Access-Control-Allow-Origin", "*"); // for develop
  res.setHeader(
    "Access-Control-Allow-Methods",
    "GET, POST, OPTIONS, PUT, PATCH, DELETE"
  );
  res.setHeader(
    "Access-Control-Allow-Headers",
    "Origin, X-Requested-With, Content-Type, Accept"
  );
  next();
});

app.use(express.json({ extended: false, limit: '35mb' }));
// app.use(express.urlencoded({ extended: true }));

// for testing server
app.get("/", (req, res) => {
  try {
    res.send("AI Gang");
  } catch (err) {
    res.status(500).json({ success: false, error: err.message });
  }
});

app.get("/test", (req, res) => {
  try {
    res.send("AI Gang");
  } catch (err) {
    res.status(500).json({ success: false, error: err.message });
  }
});

// DEFINE routes function at here...
require("./functions/review/review.routes")(app);
require("./functions/fix/fix.routes")(app);
require("./functions/webhook/webhook.routes")(app);

// BOOT server
app.listen(PORT, `${DOMAIN}`, () => {
  console.log(`Server is running on http://localhost:${PORT}`);
});

// --------------------------------------------------------------
app.all("*", (req, res, next) => {
  next(`404 Not found ${req.originalUrl} on this server`);
});

process.on("uncaughtException", (err) => {
  console.log("UNCAUGHT EXCEPTION! 💥 Shutting down...");
  console.log(err.name, err.message, err);
  process.exit(1);
});

process.on("unhandledRejection", (err) => {
  console.log("UNHANDLED REJECTION! 💥 Shutting down...");
  console.log(err);
  server.close(() => {
    process.exit(1);
  });
});
